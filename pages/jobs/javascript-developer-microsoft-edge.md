title=Javascript Developer - Microsoft Edge
desccription=eyeo is looking for a Javascript Developer focused on improving Adblock Plus for Microsoft Edge

<? include jobs/header ?>

We are looking for a developer to focus on improving Adblock Plus for Microsoft Edge. In order to do so, you should be interested in Microsoft technologies such as Edge and run Windows as your main OS.

You will also work on improving Adblock Plus to block ads more reliably and efficiently across all browsers. Strong experience with plain JavaScript and a deep understanding of web technologies is essential.

Most of our developers work remotely and so can you. We’re happy to pay for a coworking space, or if you prefer we can help you relocate closer to our offices in Cologne or Berlin.

### What we expect

* Strong experience with JavaScript
* Excellent knowledge of how the web works, from the network stack to the rendering engine, including the latest web technologies
* Experience with, and high interest in, Microsoft technologies
* Good sense for quality, simplicity and maintainability of your code

### Nice to have

* Experience with browser extension development
* Knowledge of browser internals
* Profound understanding of algorithms and data structures

Not sure if this is for you? Consider [contributing](https://adblockplus.org/en/contribute-code) first.

When applying, please include a code sample or, preferably, references to open source projects you worked on.

<? include jobs/footer ?>
