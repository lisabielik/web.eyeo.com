title=Marketing Student Help
description=eyeo needs a Marketing Student Help to assist the Flattr team with building the best online community for content creators

<? include jobs/header ?>

We create software that puts users in control over their online browsing experience.

Our products, such as Adblock Plus, Adblock Browser and Flattr, help sustain and grow a fair, open web, because they give users control while providing user-friendly monetization.

Our most popular product, Adblock Plus, is currently used on over 100 million devices.

In 2017, Flattr joined the eyeo family and we couldn't be happier! Flattr is a browser add-on that allows consumers to easily and automatically fund the content they love. To learn more, please visit flattr.com.

Our multi-cultural team wants to change the Internet for the better, and you can become an important part of it. We’re looking for someone who is passionate about building a Flattr community, engaging the right people, the right content creators and the right publishers, and bringing Flattr into the spotlight.

### Responsibilities

- You'll help us grow a great online community
- You'll work with online content creators, helping them use Flattr effectively
- You'll help with making sure processes run smoothly and that our systems are up-to-date and organized
- You'll assist in the planning and execution of campaigns

### What we expect

- You're enrolled at a German university, preferably studying subjects related to Business, Marketing, Communications or Media
- You're a native German speaker who is fluent in English. Fluency in other languages is a plus.
- You're a people person and enjoy helping others
- You're highly organized and thorough in work execution
- You're proactive, self-motivated and you get things done
- Ideally you have an interest in online content creators

<? include jobs/footer ?>
