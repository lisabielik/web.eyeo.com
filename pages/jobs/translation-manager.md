title=Translation Manager
description=We are looking for a Translation Manager to join our growing open source software team!
og_description=We are looking for a Translation Manager to join our growing open source software team! The ideal candidate is someone who is seriously passionate about language, has a keen eye for detail and is comfortable working across multiple platforms and file types. A good sense of humor is a plus. Does this sound like you? If so, we want to hear from you!

<? include jobs/header ?>

We create software that puts users in control over their online browsing experience. Our products, such as Adblock Plus, Adblock Browser and Flattr, help sustain and grow a fair, open web, because they give users control while providing user-friendly monetization. Our most popular product, Adblock Plus (<abbr title="Adblock Plus">ABP</abbr>), is currently used on over 100 million devices.

Our multi-cultural team wants to change the Internet for the better, and you can become an important part of it. We offer a competitive salary and flexible working hours. We trust our employees and believe in their ideas.

eyeo is headquartered in Cologne, Germany with satellite offices in Berlin and Malmö. However, much of our team works remotely — and so can you.

## Job description

Are you passionate about languages? Do you expect to see high quality text, no matter the language, in the software you use? Do you think that exceptional translations are the key to keeping software users happy? If you answered “YES” to these questions, we want to hear from you!

## Responsibilities

- Manage our community translations via Crowdin
- Request and review translations from our outside agency
- Manage our Translation Management System
- Liaise with our agency as well as relevant eyeo teams and stakeholders

## What we expect

- Fluent in spoken and written English
- Fluent in spoken and written German + knowledge of other European or Asian languages
- 3-5 years of experience as a Translator or Translation Manager
- Responsible and hard worker with a good sense of humor
- Ability to work in teams as well as independently
- Knowledge and interest in a variety of languages
- Sharp proofreading skills and exceptional attention to detail for various types of copy
- Ability to work across multiple platforms (desktop and mobile) and with multiple file types (`xml`, `xliff` `json`, `doc`, `xls`)
- Cultural sensitivity from a translations point of view

<? include jobs/footer ?>
