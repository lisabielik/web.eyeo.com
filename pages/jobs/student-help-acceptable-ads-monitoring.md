title=Student Help Acceptable Ads Monitoring
description=eyeo is looking for a University student based in Cologne to help monitor all things Adblock Plus.

<? include jobs/header ?>

We create software that puts users in control over their online browsing experience.

Our products, such as Adblock Plus, Adblock Browser and Flattr, help sustain and grow a fair, open web, because they give users control while providing user-friendly monetization.

Our most popular product, Adblock Plus (ABP), is currently used on over 100 million devices.

Our multi-cultural team wants to change the Internet for the better, and you can become an important part of it. We trust our employees and believe in their ideas.

For this role, you must be located in or near Cologne.

### Responsibilities

- You'll ensure that Adblock Plus is running smoothly across the web
- You'll review websites and check for compliance with agreements made
- You'll check whether ads meet the Acceptable Ads criteria on different browsers

### What we expect

- You're enrolled at a German university
- You're fluent in verbal and written English. Fluency in additional languages is an advantage.
- You're highly analytical and thorough in work execution
- You're proactive, self-motivated and you get things done

Knowledge of basic web technologies (HTML, CSS, and JavaScript) is a plus.

<? include jobs/footer ?>
